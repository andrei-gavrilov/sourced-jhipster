package ee.edu.vk.sourced.service;

import ee.edu.vk.sourced.domain.Post;
import ee.edu.vk.sourced.repository.PostRepository;
import ee.edu.vk.sourced.repository.search.PostSearchRepository;
import ee.edu.vk.sourced.security.SecurityUtils;
import org.elasticsearch.common.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.nio.file.AccessDeniedException;
import java.time.Instant;
import java.util.Optional;

import static org.elasticsearch.index.query.QueryBuilders.*;

/**
 * Service Implementation for managing Post.
 */
@Service
@Transactional
public class PostService {

    private final Logger log = LoggerFactory.getLogger(PostService.class);

    private final PostRepository postRepository;

    private final PostSearchRepository postSearchRepository;

    @Autowired
    private UserService userService;

    public PostService(PostRepository postRepository, PostSearchRepository postSearchRepository) {
        this.postRepository = postRepository;
        this.postSearchRepository = postSearchRepository;
    }

    /**
     * Save a post.
     *
     * @param post the entity to save
     * @return the persisted entity
     */
    public Post save(Post post) throws AccessDeniedException {
        log.debug("Request to save Post : {}", post);
        log.debug("Using user : {}", userService.getUserWithAuthorities().get().getLogin());
        Post result = postRepository.save(post.createdAt(Instant.now())
            .user(userService.getUserWithAuthorities().get()));
        postSearchRepository.save(result);
        return result;
    }

    //if(!post.getUser().getLogin().equals(SecurityUtils.getCurrentUserLogin().get())){
    //    log.debug("User login : {}", post.getUser().getLogin());
    //   log.debug("Selected user : {}", SecurityUtils.getCurrentUserLogin().get());
    //throw new AccessDeniedException("You are not allower to do this");
    //}


    /**
     * Get all the posts.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Post> findAll(Pageable pageable) {
        log.debug("Request to get all Posts");
        return postRepository.findAll(pageable);
    }

    /**
     * Get all the Post with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    public Page<Post> findAllWithEagerRelationships(Pageable pageable) {
        return postRepository.findAllWithEagerRelationships(pageable);
    }
    

    /**
     * Get one post by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Post> findOne(Long id) {
        log.debug("Request to get Post : {}", id);
        return postRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the post by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Post : {}", id);
        postRepository.deleteById(id);
        postSearchRepository.deleteById(id);
    }

    /**
     * Search for the post corresponding to the query.
     *
     * @param query the query of the search
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Post> search(String query, Pageable pageable) {
        log.debug("Request to search for a page of Posts for query {}", query);
        return postSearchRepository.search(queryStringQuery(query), pageable);    }
}
