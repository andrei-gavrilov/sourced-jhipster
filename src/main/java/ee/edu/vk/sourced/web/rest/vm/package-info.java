/**
 * View Models used by Spring MVC REST controllers.
 */
package ee.edu.vk.sourced.web.rest.vm;
