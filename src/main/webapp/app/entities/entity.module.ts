import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { SourcedPostModule } from './post/post.module';
import { SourcedTagModule } from './tag/tag.module';
import { SourcedCommentModule } from './comment/comment.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        SourcedPostModule,
        SourcedTagModule,
        SourcedCommentModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SourcedEntityModule {}
