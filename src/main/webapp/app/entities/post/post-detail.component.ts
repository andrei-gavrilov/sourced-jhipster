import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IPost } from 'app/shared/model/post.model';
import {Account, AccountService} from 'app/core';

@Component({
    selector: 'jhi-post-detail',
    templateUrl: './post-detail.component.html'
})
export class PostDetailComponent implements OnInit {
    post: IPost;
    account: Account;
    can_edit: boolean;
    constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute, protected accountService: AccountService) {}

    ngOnInit() {
        this.accountService.identity().then(account => {
            this.account = account;
        });
        this.activatedRoute.data.subscribe(({ post }) => {
            this.post = post;
            this.can_edit = false;
        });
        if ( this.account.login === this.post.user.login ) {
            this.can_edit = true;
        }
    }

    byteSize(field) {
        return this.dataUtils.byteSize(field);
    }

    openFile(contentType, field) {
        return this.dataUtils.openFile(contentType, field);
    }
    previousState() {
        window.history.back();
    }
}
