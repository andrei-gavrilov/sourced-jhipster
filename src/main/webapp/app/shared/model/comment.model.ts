import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';
import { IPost } from 'app/shared/model//post.model';

export interface IComment {
    id?: number;
    isCorrect?: boolean;
    content?: string;
    createdAt?: Moment;
    updatedAt?: Moment;
    user?: IUser;
    post?: IPost;
}

export class Comment implements IComment {
    constructor(
        public id?: number,
        public isCorrect?: boolean,
        public content?: string,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public user?: IUser,
        public post?: IPost
    ) {
        this.isCorrect = this.isCorrect || false;
    }
}
