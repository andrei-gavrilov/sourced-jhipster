import { Moment } from 'moment';
import { IComment } from 'app/shared/model//comment.model';
import { IUser } from 'app/core/user/user.model';
import { ITag } from 'app/shared/model//tag.model';

export interface IPost {
    id?: number;
    content?: string;
    fileContentType?: string;
    file?: any;
    createdAt?: Moment;
    updatedAt?: Moment;
    isAnswered?: boolean;
    isNsfw?: boolean;
    comments?: IComment[];
    user?: IUser;
    tags?: ITag[];
}

export class Post implements IPost {
    constructor(
        public id?: number,
        public content?: string,
        public fileContentType?: string,
        public file?: any,
        public createdAt?: Moment,
        public updatedAt?: Moment,
        public isAnswered?: boolean,
        public isNsfw?: boolean,
        public comments?: IComment[],
        public user?: IUser,
        public tags?: ITag[]
    ) {
        this.isAnswered = this.isAnswered || false;
        this.isNsfw = this.isNsfw || false;
    }
}
