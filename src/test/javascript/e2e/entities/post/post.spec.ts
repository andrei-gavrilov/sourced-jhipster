/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { PostComponentsPage, PostDeleteDialog, PostUpdatePage } from './post.page-object';
import * as path from 'path';

const expect = chai.expect;

describe('Post e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let postUpdatePage: PostUpdatePage;
    let postComponentsPage: PostComponentsPage;
    let postDeleteDialog: PostDeleteDialog;
    const fileNameToUpload = 'logo-jhipster.png';
    const fileToUpload = '../../../../../main/webapp/content/images/' + fileNameToUpload;
    const absolutePath = path.resolve(__dirname, fileToUpload);

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Posts', async () => {
        await navBarPage.goToEntity('post');
        postComponentsPage = new PostComponentsPage();
        expect(await postComponentsPage.getTitle()).to.eq('sourcedApp.post.home.title');
    });

    it('should load create Post page', async () => {
        await postComponentsPage.clickOnCreateButton();
        postUpdatePage = new PostUpdatePage();
        expect(await postUpdatePage.getPageTitle()).to.eq('sourcedApp.post.home.createOrEditLabel');
        await postUpdatePage.cancel();
    });

    it('should create and save Posts', async () => {
        const nbButtonsBeforeCreate = await postComponentsPage.countDeleteButtons();

        await postComponentsPage.clickOnCreateButton();
        await promise.all([
            postUpdatePage.setContentInput('content'),
            postUpdatePage.setFileInput(absolutePath),
            postUpdatePage.setCreatedAtInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
            postUpdatePage.setUpdatedAtInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
            postUpdatePage.userSelectLastOption()
            // postUpdatePage.tagSelectLastOption(),
        ]);
        expect(await postUpdatePage.getContentInput()).to.eq('content');
        expect(await postUpdatePage.getFileInput()).to.endsWith(fileNameToUpload);
        expect(await postUpdatePage.getCreatedAtInput()).to.contain('2001-01-01T02:30');
        expect(await postUpdatePage.getUpdatedAtInput()).to.contain('2001-01-01T02:30');
        const selectedIsAnswered = postUpdatePage.getIsAnsweredInput();
        if (await selectedIsAnswered.isSelected()) {
            await postUpdatePage.getIsAnsweredInput().click();
            expect(await postUpdatePage.getIsAnsweredInput().isSelected()).to.be.false;
        } else {
            await postUpdatePage.getIsAnsweredInput().click();
            expect(await postUpdatePage.getIsAnsweredInput().isSelected()).to.be.true;
        }
        const selectedIsNsfw = postUpdatePage.getIsNsfwInput();
        if (await selectedIsNsfw.isSelected()) {
            await postUpdatePage.getIsNsfwInput().click();
            expect(await postUpdatePage.getIsNsfwInput().isSelected()).to.be.false;
        } else {
            await postUpdatePage.getIsNsfwInput().click();
            expect(await postUpdatePage.getIsNsfwInput().isSelected()).to.be.true;
        }
        await postUpdatePage.save();
        expect(await postUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await postComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Post', async () => {
        const nbButtonsBeforeDelete = await postComponentsPage.countDeleteButtons();
        await postComponentsPage.clickOnLastDeleteButton();

        postDeleteDialog = new PostDeleteDialog();
        expect(await postDeleteDialog.getDialogTitle()).to.eq('sourcedApp.post.delete.question');
        await postDeleteDialog.clickOnConfirmButton();

        expect(await postComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
